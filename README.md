# Ghiblify

Una web para visualizar la colección de peliculas del [estudio Ghibli](https://es.wikipedia.org/wiki/Studio_Ghibli) utilizando reactJS - Redux y MaterialUI..

El diseño y estructura del proyecto se realizó para que sea adecuadamente visualizado en dispositivos de tamaño variable.

Tambien se buscó que sea facilmente customizable y escalable.

## Arquitectura

En cuanto a la arquitectura se eligió hibrida, en el sentido que se hace  una separación entre  componentes, **common components** y **containers**.

Los componentes componentes containers se contienenen las partes correspondientes a la entidad que se busca modelar, por ejemplo **/films** contiene un  archivo de estilos **Films.scss**, El componente **FilmsContainer.js**, un directorio **/components** para los componentes más especificos como **/components/FilmCard.js**, un archivo con los creadores de acciones **films.actions.js**, un archivo **index.js**, y archivos de pruebas para las acciones y el componente container **films.actions.test.js** **FilmsContainer.actions.js**

Los reducers viven en una directorio independiente al nivel general de el directorio **/src**, al igual que los servicios **/services**

## CI

El proyecto cuenta con un archivo **.gitlab-ci.yml** que ayuda a gentionar el ciclo de integración continua, en un pipeline donde se pasan las etapas de **build**, **test** y **deploy**

## Mejoras (Con más tiempo)

    1. Más pruebas unitarías para incrementar el porcentaje de cobertura del código.
    2. Agregar pruebas e2e e incluirlas en el pipeline de CI.
    3. Una pantalla de dashboard mostrando resultados relacionados a las ultimas busquedas que se hicieron.
    4. Mostrar sobre la página de detalle los recursos adicionales que vienen en la respuesta
    (pe. films y locations en vehicles) para poder ver directamente en la página de detalle ese recurso.
    5. Agregar IFrames para mostrar el trailer a la pelicula.
    6. Un buscador que mostrara imagenes relacionadas de google.
    7. Más temas

## Tabla de contenído

* [Tecnología](#tech)
* [Estructuro del proyecto](#structure)
* [Configuracion](#conf)
* [DevTools](#devTools)
* [Tests](#test)
* [Ejecutar](#run)
* [Construir](#build)

**Libraries:**

* [NodeJS](https://nodejs.org/es/)
* [React](https://facebook.github.io/react/)
* [CreateReactApp](https://github.com/facebook/create-react-app)
* [Sass](https://sass-lang.com/)
* [Jest](https://facebook.github.io/jest/)
* [MaterialUI](https://material-ui.com/)

### Transpilador JS

* [babeljs](https://babeljs.io/)

## <a name="structure"></a> Project Structure

| File name     |   Description      |
| ------------ | -------------------|
| config/          | Archivos de configuración de webpack y jest |
| node_modules/    | Modulos de  Node |
| public/      | Archivoscomo index.html, fav.icon|
| public/static/media   | Archivos estaticos como imagenes|
| public/static/locales   | Archivos de idiomas **"es"** y **"en"** |
| scripts/      | Scripts de webpack build,start y test |
| src/      | Componentes y más de la aplicación |
| src/assets     | Assets generales (iconos e imagenes)|
| src/components      | Componentes cómunes de la aplicación como Button,Card|
| src/containers      | Cómponenntes contenedores (Páginas) "La mayoría Se conectan a redux"|
| src/middlewares     | Middlewares, como logguers, o funciones para manejar las peticiones al API|
| src/reducers     | Reducers del proyecto|
| *.eslintrc*     | Configuracion de reglas para el código |
| *.gitignore*     | Patrones y nombres de archivos para excluir archivos con **git**  |
| *package.json*     |  Archivo con la lista de dependencias necesarias y otras configuraciones generales |
| *.gitlab-ci.yml*     |  Archivo con la configuración para el ciclo  de CI|
| *README.md*   | README file|
| *.env*   | Arhivo con las variables de entorno "No debería versionarse"|
## <a name="conf"></a> Configuración de entorno

* Instalar node [NVM](https://github.com/creationix/nvm#installation) o [NVM WINDOWS](https://github.com/coreybutler/nvm-windows)

```shell
  nvm install stable
```

* Instalar [YARN](https://yarnpkg.com/lang/en/docs/install/) project dependencies

## <a name="devTools"></a> DevTools

* Instalar la extención [Redux  DevTools](http://extension.remotedev.io/#installation)


## <a name="run"></a> Ejecutar el proyecto

   * intala dependecias
```shell
    yarn install
```
   * Ejecuta el proyecto
```shell
    yarn start
```

Abre tu navegador en [http://localhost:3000/](http://localhost:3000/)

## <a name="run"></a> Build project

Generar el build del  proyecto

```shell
    yarn build
```

## <a name="test"></a> Testing

Ejecutar las prubeas unitarias.

```shell
    yarn test
```

Ejecutar las pruebas  unitarias y  obtener un reporte de la covertura del código

```shell
    yarn coverage
```

Run E2E test

```shell
    yarn e2e
```

## Versioning

## Autor

* **Mauricio Campos** en [gitlab](https://gitlab.com/mauricio-cdr) y en [github](https://github.com/mauricio-cdr)
