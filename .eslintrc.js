module.exports = {
  env: {
    browser: true,
    es6: true,
    'jest/globals': true

  },
  extends: ['airbnb'],
  rules: {
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    "import/prefer-default-export": 0,
  }, globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: "babel-eslint",
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  
  plugins: [
    'react',
    'jest',
  ],
};
