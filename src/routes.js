import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import Application from './containers/App';
import Search from './containers/Search';
import Films from './containers/Films';
import Locations from './containers/Locations';
import Species from './containers/Species';
import Vehicles from './containers/Vehicles';
import People from './containers/People';
import InfoDetail from './containers/InfoDetail';


const protectedRoutes = () => null;

const publicRoutes = () => (
  <Application>
    <Switch>
      <Route exact path="/" component={Films} />
      <Route exact path="/search" component={Search} />
      <Route exact path="/films" component={Films} />
      <Route exact path="/locations" component={Locations} />
      <Route exact path="/species" component={Species} />
      <Route exact path="/vehicles" component={Vehicles} />
      <Route exact path="/people" component={People} />
      <Route exact path="/:resource/:id" component={InfoDetail} />
    </Switch>
  </Application>
);

const router = () => (
  <Router>
    <Switch>
      {publicRoutes()}
      {protectedRoutes()}
      <Route path="**" component={Films} />
    </Switch>
  </Router>
);
export default router;
