import React from 'react';
import PropTypes from 'prop-types';
import Terrain from '@material-ui/icons/Terrain';
import Film from '@material-ui/icons/Movie';
import People from '@material-ui/icons/People';
import DriveEta from '@material-ui/icons/DriveEta';
import ScatterPlot from '@material-ui/icons/ScatterPlot';

const Icon = ({ src, color }) => {
  switch (src) {
    case 'terrain':
      return <Terrain color={color} />;
    case 'local_movies':
      return <Film color={color} />;
    case 'people':
      return <People color={color} />;
    case 'directions_car':
      return <DriveEta color={color} />;
    case 'aspect_ratio':
      return <ScatterPlot color={color} />;
    default:
      return <ScatterPlot color={color} />;
  }
};

Icon.propTypes = {
  src: PropTypes.oneOf(['terrain', 'people', 'directions_car', 'aspect_ratio', 'local_movies']),
  color: PropTypes.string,
};

Icon.defaultProps = {
  src: '',
  color: 'primary',
};

export default Icon;
