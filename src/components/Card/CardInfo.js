import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CardM from '@material-ui/core/Card';
import PropTypes from 'prop-types';
import Grow from '@material-ui/core/Grow';
// import Skeleton from 'react-loading-skeleton';

const useStyles = makeStyles({
  card: {
    minWidth: 275,
    minHeight: 100,
    maxHeight: 100,
  },
});

const CardInfo = ({ title, url, onClick }) => {
  const classes = useStyles();

  return (
    <Grow in>
      <CardM className={classes.card} onClick={onClick(url)}>
        {title}
      </CardM>
    </Grow>
  );
};

CardInfo.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  onClick: PropTypes.string.isRequired,
};

export default CardInfo;
