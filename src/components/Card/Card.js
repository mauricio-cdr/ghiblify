import React, { Suspense } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CardM from '@material-ui/core/Card';
import PropTypes from 'prop-types';
import Grow from '@material-ui/core/Grow';
import Skeleton from 'react-loading-skeleton';

const useStyles = makeStyles({
  card: {
    minWidth: 275,
    minHeight: 310,
    maxHeight: 310,
    margin: 10,
  },
});

const Card = ({ children }) => {
  const classes = useStyles();

  return (
    <Grow in>
      <CardM className={classes.card}>
        <Suspense fallback={() => <Skeleton />}>
          {children}
        </Suspense>
      </CardM>
    </Grow>
  );
};

Card.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]).isRequired,
};

export default Card;
