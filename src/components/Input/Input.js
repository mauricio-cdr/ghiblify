import React from 'react';
import InputM from '@material-ui/core/Input';

export const Input = props => (
  <InputM {...props} />
);
