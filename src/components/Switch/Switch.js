import React from 'react';
import SwitchM from '@material-ui/core/Switch';

const Switch = props => <SwitchM {...props} />;

export default Switch;
