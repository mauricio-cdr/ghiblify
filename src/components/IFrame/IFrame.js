import React from 'react';
import Iframe from 'react-iframe';

const IFrame = props => <Iframe {...props} />;

export default IFrame;
