import React from 'react';
import PropTypes from 'prop-types';
import logo from '../../assets/images/ghibly_studio_logo_light.png';
import './Logo.scss';

const GhibliLogo = ({ className, style }) => (
  <img className={`Logo ${className}`} src={logo} alt="img-logo" style={style} />
);

GhibliLogo.propTypes = {
  className: PropTypes.string,
  style: PropTypes.shape({}),
};

GhibliLogo.defaultProps = {
  className: '',
  style: {},
};

export default GhibliLogo;
