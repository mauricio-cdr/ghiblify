import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import MenuIcon from '@material-ui/icons/Menu';
import Hidden from '@material-ui/core/Hidden';
import ToolbarM from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import { Collapse } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import SearchBar from '../SearchBar';
import Text from '../Text/Text';
import { FlexBox } from '../Layout';
import { menuRoutes as routes } from '../../constants/routes';

const drawerWidth = 220;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const searchElement = router => ({ resource, element }) => {
  router.push('/search');
  setTimeout(() => {
    router.push(`/${resource}/${element.id}`);
  }, 120);
};

const Toolbar = ({ history, handleDrawerToggle }) => {
  const classes = useStyles();
  const { pathname } = history.location;
  const route = routes.filter(r => pathname.search(r.link) !== -1);
  return (
    <AppBar position="fixed" className={classes.appBar} color="primary">
      <ToolbarM>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          className={classes.menuButton}
        >
          <MenuIcon />
        </IconButton>
        <FlexBox
          display="flex"
          flex="1"
          flexDirection="row"
          justifyContent="space-between"
        >
          <Collapse in>
            <Hidden xsDown implementation="css">
              <Text variant="h2">
                {route[0] ? route[0].link : ''}
              </Text>
            </Hidden>
          </Collapse>
          <SearchBar onChange={searchElement(history)} />
        </FlexBox>
      </ToolbarM>
    </AppBar>
  );
};

Toolbar.propTypes = {
  history: PropTypes.shape({
    location: PropTypes.shape({
      pathname: PropTypes.string,
    }),
  }).isRequired,
  handleDrawerToggle: PropTypes.func.isRequired,
};


Toolbar.defaultProps = {};

export default withRouter(Toolbar);
