import React, { useState } from 'react';
import Select from 'react-select';
import SearchIcon from '@material-ui/icons/Search';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Text from '../Text/Text';


const groupStyles = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
};

const groupBadgeStyles = {
  backgroundColor: '#EBECF0',
  borderRadius: '2em',
  color: '#172B4D',
  display: 'inline-block',
  fontSize: 12,
  fontWeight: 'normal',
  lineHeight: '1',
  minWidth: 1,
  padding: '0.16666666666667em 0.5em',
  textAlign: 'center',
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: 'green',
    '&:hover': {
      backgroundColor: 'yellow',
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
      minWidth: 300,
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    [theme.breakpoints.up('sm')]: {
      width: 250,
      '&:focus': {
        width: 200,
      },
    },
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 150,
      '&:focus': {
        width: 200,
      },
    },
  },
}));


const formatGroupLabel = data => (
  <div style={groupStyles}>
    <Text>{data.label || ''}</Text>
    <Text style={groupBadgeStyles}>{`${data.options.length}` || ''}</Text>
  </div>
);

const colourStyles = {
  control: styles => ({ ...styles, backgroundColor: '#FFF' }),
  option: (styles) => {
    const backgroundColor = '#999999';
    return ({
      ...styles,
      ':hover': {
        backgroundColor,
        color: '#FFF',
      },
      color: '#313131',
      cursor: 'default',
      ':active': {
        backgroundColor: '#FFF',
      },
      backgroundColor: '#FFF',
    });
  },
  input: styles => ({ ...styles }),
  placeholder: styles => ({ ...styles }),
  singleValue: styles => ({ ...styles }),
};

const getResourceType = (e) => {
  if (e.classification) return 'species';
  if (e.gender) return 'people';
  if (e.climate) return 'locations';
  if (e.director) return 'films';
  if (e.vehicle_class) return 'vehicles';

  return 'other';
};

export const SearchAppBar = ({ onChange, catalogs }) => {
  const classes = useStyles();
  const [option, setValue] = useState(null);
  const func = (element) => {
    const resource = getResourceType(element);
    setValue(element);
    onChange({ resource, element });
  };

  const groupedOptions = [{
    label: 'Vehicles',
    options: catalogs.vehicles,
  },
  {
    label: 'Films',
    options: catalogs.films,
  },
  {
    label: 'Locations',
    options: catalogs.locations,
  },
  {
    label: 'People',
    options: catalogs.people,
  },
  {
    label: 'Species',
    options: catalogs.species,
  }];

  return (
    <div className={classes.search}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <Select
        options={groupedOptions}
        formatGroupLabel={formatGroupLabel}
        value={option}
        getOptionLabel={o => (o.name || o.title)}
        placeholder="Search…"
        onChange={func}
        styles={colourStyles}
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  catalogs: {
    locations: state.get('locationsState').locations,
    vehicles: state.get('vehiclesState').vehicles,
    people: state.get('peopleState').people,
    species: state.get('speciesState').species,
    films: state.get('filmsState').films,
  },
});

SearchAppBar.propTypes = {
  onChange: PropTypes.func,
  catalogs: PropTypes.shape({}),
};

SearchAppBar.defaultProps = {
  onChange: a => a,
  catalogs: {},
};

export default connect(mapStateToProps)(SearchAppBar);
