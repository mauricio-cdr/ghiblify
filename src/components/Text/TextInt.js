import React from 'react';
import { useTranslation } from 'react-i18next';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

const TextIntl = (props) => {
  const { lkey, ...others } = props;
  const { t } = useTranslation('');
  return (
    <Typography {...others}>
      {t(lkey)}
    </Typography>
  );
};

TextIntl.propTypes = {
  lkey: PropTypes.string,
};

TextIntl.defaultProps = {
  lkey: 'key not provide',
};

export default TextIntl;
