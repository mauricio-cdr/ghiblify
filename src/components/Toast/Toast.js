import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import WarningIcon from '@material-ui/icons/Warning';
import { withStyles } from '@material-ui/core/styles';

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

const styles1 = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(2),
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

function ToastContent(props) {
  const {
    classes, className, message, onClose, variant, ...other
  } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant])}
      aria-describedby="client-snackbar"
      message={(
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      )}
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

ToastContent.propTypes = {
  classes: PropTypes.shape({
    close: PropTypes.string,
    icon: PropTypes.string,
    message: PropTypes.string,
    iconVariant: PropTypes.string,
  }).isRequired,
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

ToastContent.defaultProps = {
  className: '',
  message: '',
  onClose: a => a,
};

const ToastContentWrapper = withStyles(styles1)(ToastContent);

const styles2 = theme => ({
  margin: {
    margin: theme.spacing(2),
  },
  anchorOriginTopCenter: {
    width: '100%',
    minWidth: '100%',
  },
});

const Toast = (props) => {
  const {
    variant, open, autoHideDuration, onClose, anchorOrigin, message, classes,
  } = props;
  return (
    <Snackbar
      anchorOrigin={anchorOrigin}
      open={open}
      autoHideDuration={autoHideDuration}
      onClose={onClose}
      classes={{
        anchorOriginTopCenter: classes.anchorOriginTopCenter,
      }}
    >
      <ToastContentWrapper
        onClose={onClose}
        variant={variant}
        message={message}
      />
    </Snackbar>
  );
};

Toast.propTypes = {
  message: PropTypes.string,
  anchorOrigin: PropTypes.shape({}),
  open: PropTypes.bool,
  classes: PropTypes.shape({
    anchorOriginTopCenter: PropTypes.string,
  }).isRequired,
  autoHideDuration: PropTypes.number,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']),
};

Toast.defaultProps = {
  anchorOrigin: { vertical: 'top', horizontal: 'center' },
  open: false,
  autoHideDuration: 4000,
  onClose: a => a,
  message: '',
  variant: 'info',
};

export default withStyles(styles2)(Toast);
