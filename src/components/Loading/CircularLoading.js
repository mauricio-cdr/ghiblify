import React from 'react';
import { CircularProgress } from '@material-ui/core';

export const CircularLoading = props => (<CircularProgress {...props} />);

CircularLoading.propTypes = {};
CircularLoading.defaultProps = {};
