import React from 'react';
import { CircularLoading } from './CircularLoading';
import './Loading.scss';

export const ContainerLoading = props => (
  <div className="ContainerLoading ContainerLoading__row">
    <div className="ContainerLoading ContainerLoading__col">
      <CircularLoading {...props} color="primary" />
    </div>
  </div>
);
