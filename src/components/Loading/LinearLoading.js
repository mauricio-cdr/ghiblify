import React from 'react';
import { LinearProgress } from '@material-ui/core';

export const LinearLoading = props => (<LinearProgress {...props} />);

LinearLoading.propTypes = {};
LinearLoading.defaultProps = {};
