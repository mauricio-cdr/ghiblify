import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { makeStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import { useTranslation } from 'react-i18next';
import Text from '../Text/Text';
import { menuRoutes as options } from '../../constants/routes';
import Logo from '../Logo/GhibliLogoLight';
import DarkLogo from '../Logo/GhibliLogoDark';
import Icon from '../Icon';
import TextInt from '../Text/TextInt';
import { FlexBox, Grid } from '../Layout';
import Switch from '../Switch';
import p from '../../../package.json';
import GH_H from '../../assets/images/github_icon.png';
import GH_L from '../../assets/images/gitlab_icon.png';


import './NavBar.scss';


const renderMenuOptions = (menuOptions, history) => menuOptions.map(option => (
  <ListItem button key={`${option.lkey}`} onClick={() => { history.push(`/${option.link}`); }} color="primary">
    <ListItemIcon><Icon color="primary" src={option.icon} /></ListItemIcon>
    <Suspense fallback={`${option.lkey}`}>
      <TextInt color="primary" lkey={`nav-bar.${option.lkey}`} />
    </Suspense>
  </ListItem>
));

const drawerWidth = 220;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    marginLeft: drawerWidth,
    backgroundColor: theme.palette.primary.main,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    // backgroundColor: theme.palette.primary.main,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const NavBar = ({
  history, onChangeTheme, handleDrawerToggle, mobileOpen, isLightThemeSelected, ...others
}) => {
  const { container } = others;
  const classes = useStyles();
  const { i18n } = useTranslation();

  const drawer = (
    <div>
      <FlexBox
        display="flex"
        flexDirection="column"
        className="NavBar-header"
      >
        {!isLightThemeSelected ? <Logo style={{ height: '97px' }} /> : <DarkLogo style={{ height: '97px' }} />}
        <Text color="primary" variant="h6" align="right">{p.versionName}</Text>
      </FlexBox>
      <List>
        {renderMenuOptions(options, history)}
        <Divider light={true || true} />
        <FlexBox
          flexDirection="row"
          justifyContent="center"
          className="NavBar-label"
        >
          <TextInt variant="h4" align="center" color="primary" lkey="nav-bar.language-label" />
        </FlexBox>
        <Grid component="label" container justify="center" spacing={1}>
          <Grid item><Text color="primary">ES</Text></Grid>
          <Grid item style={{ marginTop: '-10px' }}>
            <Switch
              color="secondary"
              onChange={(e) => {
                if (e.target.checked) {
                  i18n.changeLanguage('en-US');
                  return;
                }
                i18n.changeLanguage('es-US');
              }}
            />
          </Grid>
          <Grid item><Text color="primary">EN</Text></Grid>
        </Grid>
        <FlexBox
          flexDirection="row"
          justifyContent="center"
          className="NavBar-label"
        >
          <TextInt variant="h4" align="center" color="primary" lkey="nav-bar.theme-label" />
        </FlexBox>
        <Grid component="label" container justify="center" spacing={1}>
          <Grid item><Text color="primary">T1</Text></Grid>
          <Grid item style={{ marginTop: '-10px' }}>
            <Switch
              color="primary"
              onChange={(e) => {
                onChangeTheme(e.target.checked);
              }}
            />
          </Grid>
          <Grid item><Text color="primary">T2</Text></Grid>
        </Grid>
        <div style={{ height: '30px' }} />
        <FlexBox display="flex" flexDirection="row" justifyContent="center">
          <a href="https://gitlab.com/mauricio-cdr"><img src={GH_L} style={{ width: '80px' }} alt="gitlab_logo" /></a>
        </FlexBox>
        <div style={{ height: '10px' }} />
        <FlexBox display="flex" flexDirection="row" justifyContent="center">
          <a href="https://github.com/mauricio-cdr"><img src={GH_H} style={{ width: '50px' }} alt="github_logo" /></a>
        </FlexBox>
      </List>
    </div>
  );

  return (
    <div className={classes.root}>
      <nav className={classes.drawer} aria-label="mailbox folders">
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor="left"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true,
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </div>
  );
};


NavBar.propTypes = {
  history: PropTypes.shape({}).isRequired,
  onChangeTheme: PropTypes.func.isRequired,
  mobileOpen: PropTypes.bool.isRequired,
  handleDrawerToggle: PropTypes.func.isRequired,
  isLightThemeSelected: PropTypes.bool.isRequired,
};

export default withRouter(NavBar);
