import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers/index';
import promiseMiddleware from './middlewares/promise.middleware';


export default (initialState = null) => {
  /* eslint-disable */
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  /* eslint-enable */
  let store = {};
  if (process.env.REACT_APP_ENV === 'dev') {
    const enhancer = compose(composeEnhancers(applyMiddleware(promiseMiddleware)));
    store = createStore(rootReducer, initialState, enhancer);
  } else if (initialState) {
    store = createStore(rootReducer,
      initialState, composeEnhancers(applyMiddleware(promiseMiddleware)));
  } else {
    store = createStore(rootReducer, composeEnhancers(applyMiddleware(promiseMiddleware)));
  }
  return { store };
};
