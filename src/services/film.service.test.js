import axios from 'axios';
import sinon from 'sinon';
import { fetchFilms, fetchFilmById } from './film.service';
import { GHIBLI_API_URL } from '../constants/api';


describe('FilmService > film.service', () => {
  let axiosSpy;
  const sandbox = sinon.createSandbox();

  afterEach(() => {
    sandbox.restore();
  });

  it('Should call to films list service once', () => {
    const url = `${GHIBLI_API_URL}/films`;
    axiosSpy = sandbox.spy(axios, 'get');
    expect(fetchFilms()).toBeInstanceOf(Promise);
    expect(axiosSpy.withArgs(url).calledOnce).toBe(true);
  });

  it('Should call to films by id service once', () => {
    const id = '123-abc-123';
    const url = `${GHIBLI_API_URL}/films/${id}`;

    axiosSpy = sandbox.spy(axios, 'get');
    expect(fetchFilmById({ params: { id } })).toBeInstanceOf(Promise);
    expect(axiosSpy.withArgs(url).calledOnce).toBe(true);
  });
});
