import axios from 'axios';
import sinon from 'sinon';
import { fetchSpecieById, fetchSpecies } from './specie.service';
import { GHIBLI_API_URL } from '../constants/api';


describe('SpeciesService > species.service', () => {
  let axiosSpy;
  const sandbox = sinon.createSandbox();

  afterEach(() => {
    sandbox.restore();
  });

  it('Should call to species list service once', () => {
    const url = `${GHIBLI_API_URL}/species`;
    axiosSpy = sandbox.spy(axios, 'get');
    expect(fetchSpecies()).toBeInstanceOf(Promise);
    expect(axiosSpy.withArgs(url).calledOnce).toBe(true);
  });

  it('Should call to films by id service once', () => {
    const id = '123-abc-123';
    const url = `${GHIBLI_API_URL}/species/${id}`;
    axiosSpy = sandbox.spy(axios, 'get');
    expect(fetchSpecieById({ params: { id } })).toBeInstanceOf(Promise);
    expect(axiosSpy.withArgs(url).calledOnce).toBe(true);
  });
});
