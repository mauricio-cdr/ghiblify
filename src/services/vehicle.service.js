import axios from 'axios';
import { GHIBLI_API_URL } from '../constants/api';


export const fetchVehicles = () => axios.get(
  `${GHIBLI_API_URL}/vehicles`,
);

export const fetchVehicleById = req => axios.get(
  `${GHIBLI_API_URL}/vehicles/${req.params.id}`,
);
