import axios from 'axios';
import { GHIBLI_API_URL } from '../constants/api';

export const fetchPeople = () => axios.get(
  `${GHIBLI_API_URL}/people`,
);

export const fetchPersonById = req => axios.get(
  `${GHIBLI_API_URL}/people/${req.params.id}`,
);
