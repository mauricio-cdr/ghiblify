import axios from 'axios';
import { GHIBLI_API_URL } from '../constants/api';


export const fetchFilms = () => axios.get(
  `${GHIBLI_API_URL}/films`,
);

export const fetchFilmById = req => axios.get(
  `${GHIBLI_API_URL}/films/${req.params.id}`,
);
