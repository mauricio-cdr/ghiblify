import axios from 'axios';
import sinon from 'sinon';
import { fetchPeople, fetchPersonById } from './people.service';
import { GHIBLI_API_URL } from '../constants/api';


describe('PeopleService > people.service', () => {
  let axiosSpy;
  const sandbox = sinon.createSandbox();

  afterEach(() => {
    sandbox.restore();
  });

  it('Should call to people list service once', () => {
    const url = `${GHIBLI_API_URL}/people`;
    axiosSpy = sandbox.spy(axios, 'get');
    expect(fetchPeople()).toBeInstanceOf(Promise);
    expect(axiosSpy.withArgs(url).calledOnce).toBe(true);
  });

  it('Should call to people by id service once', () => {
    const id = '123-abc-123';
    const url = `${GHIBLI_API_URL}/people/${id}`;

    axiosSpy = sandbox.spy(axios, 'get');
    expect(fetchPersonById({ params: { id } })).toBeInstanceOf(Promise);
    expect(axiosSpy.withArgs(url).calledOnce).toBe(true);
  });
});
