import axios from 'axios';
import { queryUrl } from '../helpers/format/format';
import { fetchFilms } from './film.service';
import { fetchLocations } from './location.service';
import { fetchPeople } from './people.service';
import { fetchSpecies } from './specie.service';
import { fetchVehicles } from './vehicle.service';
import { GHIBLI_API_URL } from '../constants/api';


export const fetchResourses = req => axios.get(
  `${GHIBLI_API_URL}/${req.params.resource}?${queryUrl.stringify(req.params.url_params)}`,
);

export const fetchResourceById = req => axios.get(
  `${GHIBLI_API_URL}/${req.params.resource}/${req.params.id}`,
);

export const fetchAllResources = () => Promise.all([
  fetchFilms().then(a => ({ resource: 'films', ...a })),
  fetchPeople().then(a => ({ resource: 'people', ...a })),
  fetchSpecies().then(a => ({ resource: 'species', ...a })),
  fetchLocations().then(a => ({ resource: 'locations', ...a })),
  fetchVehicles().then(a => ({ resource: 'vehicles', ...a })),
]);
