import axios from 'axios';
import sinon from 'sinon';
import { fetchResourses, fetchResourceById } from './search.service';
import { GHIBLI_API_URL } from '../constants/api';


describe('SearchService > search.service', () => {
  let axiosSpy;
  const sandbox = sinon.createSandbox();

  afterEach(() => {
    sandbox.restore();
  });

  it('Should to call search resource list service once', () => {
    const url = `${GHIBLI_API_URL}/films?`;
    axiosSpy = sandbox.spy(axios, 'get');
    expect(fetchResourses({ params: { resource: 'films' } })).toBeInstanceOf(Promise);
    expect(axiosSpy.withArgs(url).calledOnce).toBe(true);
  });

  it('Should to call search resource by id service once', () => {
    const id = '123-abc-123';
    const url = `${GHIBLI_API_URL}/films/${id}`;

    axiosSpy = sandbox.spy(axios, 'get');
    expect(fetchResourceById({ params: { id, resource: 'films' } })).toBeInstanceOf(Promise);
    expect(axiosSpy.withArgs(url).calledOnce).toBe(true);
  });
});
