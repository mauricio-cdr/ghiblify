import axios from 'axios';
import sinon from 'sinon';
import { fetchLocations } from './location.service';
import { GHIBLI_API_URL } from '../constants/api';


describe('LocationsService > locations.service', () => {
  let axiosSpy;
  const sandbox = sinon.createSandbox();

  afterEach(() => {
    sandbox.restore();
  });

  it('Should call to locations list service once', () => {
    const url = `${GHIBLI_API_URL}/locations`;
    axiosSpy = sandbox.spy(axios, 'get');
    expect(fetchLocations()).toBeInstanceOf(Promise);
    expect(axiosSpy.withArgs(url).calledOnce).toBe(true);
  });
});
