import axios from 'axios';
import sinon from 'sinon';
import { fetchVehicles, fetchVehicleById } from './vehicle.service';
import { GHIBLI_API_URL } from '../constants/api';


describe('VehiclesService > vehicle.service', () => {
  let axiosSpy;
  const sandbox = sinon.createSandbox();

  afterEach(() => {
    sandbox.restore();
  });

  it('Should call to vehicles list service once', () => {
    const url = `${GHIBLI_API_URL}/vehicles`;
    axiosSpy = sandbox.spy(axios, 'get');
    expect(fetchVehicles()).toBeInstanceOf(Promise);
    expect(axiosSpy.withArgs(url).calledOnce).toBe(true);
  });

  it('Should call to vehicles by id service once', () => {
    const id = '123-abc-123';
    const url = `${GHIBLI_API_URL}/vehicles/${id}`;

    axiosSpy = sandbox.spy(axios, 'get');
    expect(fetchVehicleById({ params: { id } })).toBeInstanceOf(Promise);
    expect(axiosSpy.withArgs(url).calledOnce).toBe(true);
  });
});
