import axios from 'axios';
import { GHIBLI_API_URL } from '../constants/api';

export const fetchSpecies = () => axios.get(
  `${GHIBLI_API_URL}/species`,
);

export const fetchSpecieById = req => axios.get(
  `${GHIBLI_API_URL}/species/${req.params.id}`,
);
