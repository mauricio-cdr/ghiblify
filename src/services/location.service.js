import axios from 'axios';
import { GHIBLI_API_URL } from '../constants/api';

export const fetchLocations = () => axios.get(
  `${GHIBLI_API_URL}/locations`,
);
