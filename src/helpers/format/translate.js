
import { useTranslation } from 'react-i18next';

export default (lkey) => {
  const { t } = useTranslation('');
  return (t(lkey));
};
