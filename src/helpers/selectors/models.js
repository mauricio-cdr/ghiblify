export const people = e => ({
  name: e.name,
  gender: e.gender,
  age: e.age,
  eye_color: e.eye_color,
  hair_color: e.hair_color,
});

export const vehicles = e => ({
  name: e.name,
  description: e.description,
  vehicle_class: e.vehicle_class,
  length: e.length,
});

export const species = e => ({
  name: e.name,
  classification: e.description,
  eye_colors: e.eye_colors,
  hair_colors: e.hair_color,
});

export const films = e => ({
  rt_score: e.rt_score,
  title: e.title,
  release_date: e.release_date,
  length: e.length,
  director: e.director,
  producer: e.producer,
  description: e.description,
});

export const locations = e => ({
  name: e.name,
  climate: e.climate,
  terrain: e.terrain,
  surface_water: e.surface_water,
  length: e.length,
});


export const resources = {
  people,
  locations,
  films,
  vehicles,
  species,
};
