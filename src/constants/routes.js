export const menuRoutes = [
  {
    lkey: 'menu-item-films',
    link: 'films',
    icon: 'local_movies',
  },
  {
    lkey: 'menu-item-locations',
    link: 'locations',
    icon: 'terrain',
  },
  {
    lkey: 'menu-item-people',
    link: 'people',
    icon: 'people',
  },
  {
    lkey: 'menu-item-vehicles',
    link: 'vehicles',
    icon: 'directions_car',
  },
  {
    lkey: 'menu-item-species',
    link: 'species',
    icon: 'aspect_ratio',
  },
];
