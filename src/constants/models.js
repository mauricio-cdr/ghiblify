export const models = {
  people: ['name', 'gender', 'age', 'eye_color', 'hair_color'],
  vehicles: ['name', 'description', 'vehicle_class', 'length'],
  species: ['name', 'classification', 'eye_colors', 'hair_colors'],
  films: ['rt_score', 'title', 'release_date', 'length', 'director', 'producer', 'description'],
  locations: ['name : Gutiokipanja', 'climate', 'terrain', 'surface_water', 'length'],
};
