import dashboardActions from './dashboard.action-types';
import errorsActions from './errors.action-types';
import homeActions from './home.action-types';
import locationsActions from './locations.action-types';
import notificationsActions from './notifications.action-types';
import speciesActions from './species.action-types';
import vehiclesActions from './vehicles.action-types';

export {
  dashboardActions,
  errorsActions,
  homeActions,
  locationsActions,
  notificationsActions,
  speciesActions,
  vehiclesActions,
};
