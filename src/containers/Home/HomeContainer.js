import React from 'react';
import { connect } from 'react-redux';

export class HomeContainer extends React.Component {
  componentDidMount() {

  }

  render() {
    return (
      <div>
        Home
      </div>
    );
  }
}


export default connect(
  state => ({
    authState: state.get('homeState'),
  }),
)(HomeContainer);
