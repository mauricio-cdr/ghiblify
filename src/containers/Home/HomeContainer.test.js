import React from 'react';
import { shallow } from 'enzyme';
import { HomeContainer } from './HomeContainer';

describe('<Home />', () => {
  test('renders without crashing', () => {
    const props = {};
    const component = shallow(
      <HomeContainer {...props} />,
    );
    expect(component).toMatchSnapshot();
  });
});
