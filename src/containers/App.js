import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { MuiThemeProvider } from '@material-ui/core/styles';
import NavBar from '../components/NavBar';
import { FlexBox } from '../components/Layout';
import Toast from '../components/Toast';
import { ContainerLoading } from '../components/Loading';
import Toolbar from '../components/Toolbar';
import { darkTheme, lightTheme } from './Theme';
import { getAllResources } from './Search/search.actions';
import './App.scss';


export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLightThemeSelected: false,
      isOpenDrawer: false,
    };
  }

  componentDidMount() {
    const { getResources } = this.props;
    getResources();
  }


  handleDrawerToggle = () => {
    const { isOpenDrawer } = this.state;
    this.setState({ isOpenDrawer: !isOpenDrawer });
  }

  handleOnChangeTheme = isLightThemeSelected => this.setState({ isLightThemeSelected });

  render() {
    const { isLightThemeSelected, isOpenDrawer } = this.state;
    const { children } = this.props;
    return (
      <MuiThemeProvider theme={isLightThemeSelected ? lightTheme : darkTheme}>
        <div className="App">
          <Suspense
            fallback={(
              <div style={{ width: 220, height: '100%' }}>
                <ContainerLoading thickness={7} size={40} />
              </div>
            )}
          >
            <NavBar
              handleDrawerToggle={this.handleDrawerToggle}
              onChangeTheme={this.handleOnChangeTheme}
              mobileOpen={isOpenDrawer}
              isLightThemeSelected={isLightThemeSelected}
            />
          </Suspense>
          <FlexBox flexDirection="column" flex="1">
            <Toolbar handleDrawerToggle={this.handleDrawerToggle} />
            <div className="App-body">
              <Suspense fallback={<ContainerLoading thickness={7} size={90} />}>
                {children}
              </Suspense>
            </div>
          </FlexBox>
        </div>
        <Toast />
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  children: PropTypes.element.isRequired,
  getResources: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  getResources: () => dispatch(getAllResources()),
});
export default connect(null, mapDispatchToProps)(App);
