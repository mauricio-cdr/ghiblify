import { createMuiTheme } from '@material-ui/core/styles';

const typography = {
  useNextVariants: true,
  fontFamily: [
    'Roboto',
    'Helvetica',
  ].join(','),
  fontWeightMedium: 500,
  h1: {
    fontSize: '2.4rem',
    fontWeight: 'bold',
    lineHeight: '2px',
  },
  h2: {
    fontSize: '1.3rem',
    fontWeight: 'bold',
    lineHeight: '30px',
  },
  h3: {
    fontSize: '1rem',
    fontWeight: 'bold',
    lineHeight: '18px',
  },
  h4: {
    fontSize: '.9rem',
    lineHeight: '16px',
  },
  h5: {
    fontSize: '.8rem',
    fontWeight: 'bold',
    lineHeight: '13px',
  },
  h6: {
    fontSize: '.7rem',
    fontWeight: 'bold',
    lineHeight: '11px',
  },
  display1: {
    fontSize: '13px',
    lineHeight: '16px',
    fontWeight: 'bold !important',
  },
  overline: {
    fontSize: '11px',
    lineHeight: '13px',
  },
  button: {
    fontSize: '13px',
    lineHeight: '16px',
    textAlign: 'center',
    fontWeight: 'normal',
  },
};

export const lightTheme = createMuiTheme({
  palette: {
    primary: {
      light: '#D1C4E9',
      main: '#673AB7',
      dark: '#512DA8',
      contrastText: '#FFFFFF',
    },
    secondary: {
      light: '#00BCD4',
      main: '#00BCD4',
      dark: '#00BCD4',
      contrastText: '#FFF',
    },
  },
  typography,
});

export const darkTheme = createMuiTheme({
  palette: {
    primary: {
      light: '#484848',
      main: '#212121',
      dark: '#000000',
      contrastText: '#FFFFFF',
    },
    secondary: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
      contrastText: '#FFF',
    },
  },
  typography,
});
