import React from 'react';
import { shallow } from 'enzyme';
import { SpeciesContainer } from './SpeciesContainer';

describe('<SpeciesContainer />', () => {
  test('renders without crashing', () => {
    const props = {
      getSpecies: a => a,
      species: [],
      history: {},
    };
    const component = shallow(
      <SpeciesContainer {...props} />,
    );
    expect(component).toMatchSnapshot();
  });
});
