import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Grid } from '../../components/Layout';
import SpecieCard from './components/SpecieCard';


import { getSpecies as fetchSpecies } from './species.actions';

export class SpeciesContainer extends React.Component {
  componentDidMount() {
    const { getSpecies } = this.props;
    getSpecies();
  }

  handleOnClickCard = specie => () => {
    const { history } = this.props;
    history.push(`/species/${specie.id}`);
  }

  renderSpecies = (species) => {
    if (!species) return null;
    return species.map(specie => (
      <Grid item key={specie.id} xs>
        <SpecieCard data={specie} onClick={this.handleOnClickCard} />
      </Grid>
    ));
  }

  render() {
    const { species } = this.props;
    return (
      <Grid
        container
        direction="row"
        justify="space-around"
      >
        {this.renderSpecies(species)}
      </Grid>
    );
  }
}

SpeciesContainer.propTypes = {
  getSpecies: PropTypes.func.isRequired,
  species: PropTypes.arrayOf(PropTypes.shape({})),
  history: PropTypes.shape({}).isRequired,
};

SpeciesContainer.defaultProps = {
  species: [],
};

const mapStateToProps = state => ({
  species: state.get('speciesState').species,
  isLoading: state.get('speciesState').isLoading,
});

const mapDispatchToProps = dispatch => ({
  getSpecies: () => dispatch(fetchSpecies()),
});
export default connect(mapStateToProps, mapDispatchToProps)(SpeciesContainer);
