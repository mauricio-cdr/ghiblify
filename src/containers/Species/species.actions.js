import { fetchSpecies } from '../../services/specie.service';
import * as ACTIONS from '../../constants/actions/species.action-types';

export const getSpecies = () => ({
  types: [
    ACTIONS.LOADING_FETCH_SPECIES,
    ACTIONS.SUCCESS_FETCH_SPECIES,
    ACTIONS.ERROR_FETCH_SPECIES,
  ],
  promise: fetchSpecies(),
});

export const searchSpecies = () => ({
  types: [
    ACTIONS.LOADING_FETCH_SPECIES,
    ACTIONS.SUCCESS_FETCH_SPECIES,
    ACTIONS.ERROR_FETCH_SPECIES,
  ],
  promise: fetchSpecies(),
});
