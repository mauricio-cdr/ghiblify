import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '../../../components/Button';
import TextIntl from '../../../components/Text/TextInt';
import Text from '../../../components/Text/Text';
import Card from '../../../components/Card';
import t from '../../../helpers/format/translate';

const useStyles = makeStyles({
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  pos: {
    marginBottom: 5,
  },
  release: {
    color: 'green',
  },
  media: {
    height: 140,
  },
});


const SpecieCard = ({ data, onClick }) => {
  const classes = useStyles();

  return (
    <Card>
      <CardContent>
        <Text variant="h2" color="textSecondary" gutterBottom>
          {data.name}
        </Text>
        <Text className={classes.pos} variant="h5" color="textSecondary">
          {`${t('species.card.classification')}: ${data.classification}`}
        </Text>
        <Text className={classes.pos} variant="h5" color="textSecondary">
          {`${t('species.card.eye_colors')}: ${data.eye_colors}`}
        </Text>
        <Text className={classes.pos} variant="h5" color="textSecondary">
          {`${t('species.card.hair_colors')}: ${data.hair_colors}`}
        </Text>
      </CardContent>
      <CardActions>
        <Button size="medium" color="secondary" onClick={onClick(data)}>
          <TextIntl lkey="locations.card.read-more" onClick={onClick(data)} />
        </Button>
      </CardActions>
    </Card>
  );
};

SpecieCard.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    classification: PropTypes.string,
    eye_colors: PropTypes.string,
    hair_colors: PropTypes.string,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default SpecieCard;
