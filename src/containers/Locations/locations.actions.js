import { fetchLocations } from '../../services/location.service';
import * as ACTIONS from '../../constants/actions/locations.action-types';

export const getLocations = () => ({
  types: [
    ACTIONS.LOADING_FETCH_LOCATIONS,
    ACTIONS.SUCCESS_FETCH_LOCATIONS,
    ACTIONS.ERROR_FETCH_LOCATIONS,
  ],
  promise: fetchLocations(),
});

export const searchLocations = () => ({
  types: [
    ACTIONS.LOADING_FETCH_LOCATIONS,
    ACTIONS.SUCCESS_FETCH_LOCATIONS,
    ACTIONS.ERROR_FETCH_LOCATIONS,
  ],
  promise: fetchLocations(),
});
