import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Grid } from '../../components/Layout';
import LocationCard from './components/LocationCard';
import { getLocations as fetchLocations } from './locations.actions';

export class LocationsContainer extends React.Component {
  componentDidMount() {
    const { getLocations } = this.props;
    getLocations();
  }

  handleOnClickCard = location => () => {
    const { history } = this.props;
    history.push(`/locations/${location.id}`);
  }

  renderLocations = (locations) => {
    if (!locations) return null;
    return locations.map(location => (
      <Grid item key={location.id} xs>
        <LocationCard data={location} onClick={this.handleOnClickCard} />
      </Grid>
    ));
  }

  render() {
    const { locations } = this.props;
    return (
      <Grid
        container
        direction="row"
        justify="space-around"
      >
        {this.renderLocations(locations)}
      </Grid>
    );
  }
}

LocationsContainer.propTypes = {
  getLocations: PropTypes.func.isRequired,
  locations: PropTypes.arrayOf(PropTypes.shape({})),
  history: PropTypes.shape({}).isRequired,
};

LocationsContainer.defaultProps = {
  locations: [],
};

const mapStateToProps = state => ({
  locations: state.get('locationsState').locations,
  loading: state.get('locationsState').loading,
});

const mapDispatchToProps = dispatch => ({
  getLocations: () => dispatch(fetchLocations()),
});
export default connect(mapStateToProps, mapDispatchToProps)(LocationsContainer);
