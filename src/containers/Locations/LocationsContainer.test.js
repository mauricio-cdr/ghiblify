import React from 'react';
import { shallow } from 'enzyme';
import { LocationsContainer } from './LocationsContainer';

describe('<LocationsContainer />', () => {
  test('renders without crashing', () => {
    const props = {
      getLocations: a => a,
      locations: [],
      history: {},
    };
    const component = shallow(
      <LocationsContainer {...props} />,
    );
    expect(component).toMatchSnapshot();
  });
});
