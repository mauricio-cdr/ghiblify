import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import Text from '../../../components/Text/Text';
import Card from '../../../components/Card';
import Button from '../../../components/Button';
import TextIntl from '../../../components/Text/TextInt';
import t from '../../../helpers/format/translate';


import { terrainImg } from './TerrainImg';


const useStyles = makeStyles({
  card: {
    minWidth: 275,
    maxWidth: 300,
    minHeight: 310,
    maxHeight: 310,
    margin: 10,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  pos: {
    marginBottom: 5,
  },
  release: {
    color: 'green',
  },
  media: {
    height: 140,
  },
});


const LocationsCard = ({ data, onClick }) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardMedia
        className={classes.media}
        image={terrainImg(data.terrain)}
        title={data.terrain}
      />
      <CardContent>
        <Text variant="h2" color="textSecondary" gutterBottom>
          {data.name}
        </Text>
        <Text className={classes.pos} variant="h5" color="textSecondary">
          {`${t('locations.card.climate')}: ${data.climate}`}
        </Text>
        <Text className={classes.pos} variant="h5" color="textSecondary">
          {`${t('locations.card.terrain')}: ${data.terrain}`}
        </Text>
        <Text className={classes.pos} variant="h5" color="textSecondary">
          {`${t('locations.card.surface_water')}: ${data.surface_water}`}
        </Text>
      </CardContent>
      <CardActions>
        <Button size="medium" color="secondary" onClick={onClick(data)}>
          <TextIntl lkey="locations.card.read-more" onClick={onClick(data)} />
        </Button>
      </CardActions>
    </Card>
  );
};

LocationsCard.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    name: PropTypes.string,
    climate: PropTypes.string,
    surface_water: PropTypes.string,
    terrain: PropTypes.string,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default LocationsCard;
