
import forestImg from '../../../assets/images/forest.jpg';
import hillsImg from '../../../assets/images/hills.jpg';
import marshImg from '../../../assets/images/marsh.jpg';
import mountainImg from '../../../assets/images/mountain.jpg';
import notFoundImg from '../../../assets/images/nfound.png';
import oceanImg from '../../../assets/images/ocean.jpg';
import plainImg from '../../../assets/images/plain.jpg';
import cityImg from '../../../assets/images/city.jpg';
import riverImg from '../../../assets/images/river.jpg';


const HILLS = 'Hill';
const CITY = 'City';
const FOREST = 'Forest';
const OCEAN = 'Ocean';
const MARSH = 'Marsh';
const PLAIN = 'Plain';
const MOUNTAIN = 'Mountain';
const RIVER = 'River';


export const terrainImg = (type) => {
  switch (type) {
    case HILLS: { return hillsImg; }
    case CITY: { return cityImg; }
    case FOREST: { return forestImg; }
    case OCEAN: { return oceanImg; }
    case MARSH: { return marshImg; }
    case PLAIN: { return plainImg; }
    case MOUNTAIN: { return mountainImg; }
    case RIVER: { return riverImg; }
    default: { return notFoundImg; }
  }
};
