import { fetchResourses, fetchAllResources } from '../../services/search.service';
import * as ACTIONS from '../../constants/actions/search.action-types';

export const searchResourse = req => ({
  types: [
    ACTIONS.LOADING_FETCH_SEARCH,
    ACTIONS.SUCCESS_FETCH_SEARCH,
    ACTIONS.ERROR_FETCH_SEARCH,
  ],
  promise: fetchResourses(req),
});


export const getAllResources = () => ({
  types: [
    ACTIONS.LOADING_FETCH_RESOURCES,
    ACTIONS.SUCCESS_FETCH_RESOURCES,
    ACTIONS.ERROR_FETCH_RESOURCES,
  ],
  promise: fetchAllResources(),
});
