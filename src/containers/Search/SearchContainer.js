import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Skeleton from 'react-loading-skeleton';
import Logo from '../../components/Logo/GhibliLogoLight';
import { FlexBox, Grid } from '../../components/Layout';
import { getAllResources as fetchAllResources } from './search.actions';
import './Search.scss';

export class SearchContainer extends React.Component {
  componentDidMount() {}

  render() {
    return (
      <FlexBox
        display="flex"
        flexDirection="column"
        className="container"
      >
        <Grid container>
          <Grid item xs={12} md={6}>
            <Logo className="FilmDetail-img" />
          </Grid>
          <Grid item xs={12} md={6}>
            <FlexBox
              display="flex"
              flexDirection="column"
              justifyContent="flex-end"
              className="SearchContainer-info"
            >
              <Grid item xs={5} lg={5} md={5}>
                <Skeleton count={4} />
              </Grid>
            </FlexBox>
          </Grid>
        </Grid>
        <Grid item xs className="SearchContainer-description">
          <Skeleton count={4} />
        </Grid>
        <div className="SearchContainer-view-wiki" />
      </FlexBox>
    );
  }
}

SearchContainer.propTypes = {
  history: PropTypes.shape({
    location: PropTypes.shape({
      search: PropTypes.string,
    }),
  }).isRequired,
};


const mapStateToProps = state => ({
  results: state.get('searchState').results,
  isLoading: state.get('searchState').isLoadingResults,
});

const mapDispatchToProps = dispatch => ({
  getAllResources: () => dispatch(fetchAllResources()),
});
export default connect(mapStateToProps, mapDispatchToProps)(SearchContainer);
