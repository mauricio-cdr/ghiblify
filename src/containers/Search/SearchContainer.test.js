import React from 'react';
import { shallow } from 'enzyme';
import { SearchContainer } from './SearchContainer';

describe('<SearchContainer />', () => {
  test('renders without crashing', () => {
    const props = {
      history: {},
    };
    const component = shallow(
      <SearchContainer {...props} />,
    );
    expect(component).toMatchSnapshot();
  });
});
