import { fetchPeople } from '../../services/people.service';
import * as ACTIONS from '../../constants/actions/people.action-types';

export const getPeople = req => ({
  types: [
    ACTIONS.LOADING_FETCH_PEOPLE,
    ACTIONS.SUCCESS_FETCH_PEOPLE,
    ACTIONS.ERROR_FETCH_PEOPLE,
  ],
  promise: fetchPeople(req),
});
