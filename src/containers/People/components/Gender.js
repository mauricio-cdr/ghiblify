import React from 'react';
import PropTypes from 'prop-types';
import femaleGenderImg from '../../../assets/images/gender-female.png';
import maleGenderImg from '../../../assets/images/gender-male.png';
import unknownGenderImg from '../../../assets/images/gender-unknown.png';


const MALE_GENDER = 'male';
const FEMALE_GENDER = 'female';

const Gender = ({ gender }) => {
  const style = {
    card: {
      width: 15,
      height: 'auto',
      marginTop: -5,
    },
    img: {
      width: 15,
      height: 'auto',
    },
  };
  const genderL = gender.toLowerCase();
  return (
    <div style={style.card}>
      { genderL === MALE_GENDER && <img src={maleGenderImg} alt="male-gender-img" style={style.img} />}
      { genderL === FEMALE_GENDER && <img src={femaleGenderImg} alt="female-gender-img" style={style.img} />}
      { genderL !== MALE_GENDER && genderL !== FEMALE_GENDER && <img src={unknownGenderImg} alt="unknown-gender-img" style={style.img} />}
    </div>
  );
};

Gender.propTypes = {
  gender: PropTypes.string.isRequired,
};

export default Gender;
