import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '../../../components/Button';
import TextIntl from '../../../components/Text/TextInt';
import Text from '../../../components/Text/Text';
import { FlexBox } from '../../../components/Layout';
import Card from '../../../components/Card';
import t from '../../../helpers/format/translate';
import Eye from './Eye';
import Gender from './Gender';
import './PersonCard.scss';

const useStyles = makeStyles({
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  pos: {
    marginBottom: 5,
  },
  release: {
    color: 'green',
  },
  media: {
    height: 140,
  },
});


const LocationsCard = ({ data, onClick }) => {
  const classes = useStyles();

  return (
    <Card>
      <CardContent>
        <FlexBox>
          <Text variant="h2" color="textSecondary" gutterBottom>
            {data.name}
          </Text>
        </FlexBox>
        <FlexBox>
          <Text variant="h4" color="textSecondary" gutterBottom>
            {`${t('people.card.eye_color')}: ${data.eye_color}`}
          </Text>
          <Eye color={data.eye_color} />
        </FlexBox>
        <FlexBox>
          <Text className={classes.pos} variant="h5" color="textSecondary">
            {`${t('people.card.hair_color')}: ${data.hair_color}`}
          </Text>
          <div className="People.PersonCard-shape" style={{ backgroundColor: data.hair_color }} />
        </FlexBox>
        <FlexBox>
          <Text className={classes.pos} variant="h5" color="textSecondary">
            {`${t('people.card.age')}: ${data.age}`}
          </Text>
        </FlexBox>
        <FlexBox flexDirection="row" display="flex">
          <Text className={classes.pos} variant="h5" color="textSecondary">
            {t('people.card.gender')}
          </Text>
          <div style={{ minWidth: '10px' }} />
          <Gender gender={data.gender} />
        </FlexBox>
      </CardContent>
      <CardActions>
        <Button size="medium" color="secondary" onClick={onClick(data)}>
          <TextIntl lkey="locations.card.read-more" onClick={onClick(data)} />
        </Button>
      </CardActions>
    </Card>
  );
};

LocationsCard.propTypes = {
  data: PropTypes.shape({
    age: PropTypes.string,
    gender: PropTypes.string,
    hair_color: PropTypes.string,
    name: PropTypes.string,
    eye_color: PropTypes.string,
    species: PropTypes.string,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default LocationsCard;
