import React from 'react';
import PropTypes from 'prop-types';
import eye from '../../../assets/images/eye.png';

const Eye = ({ color }) => {
  const style = {
    card: {
      width: 30,
      marginTop: -3,
      height: 'auto',
    },
    img: {
      width: 30,
      height: 'auto',
      backgroundColor: `${color}`,
    },
  };
  return (
    <div style={style.card}>
      <img src={eye} alt="eye-img" style={style.img} />
    </div>
  );
};

Eye.propTypes = {
  color: PropTypes.string.isRequired,
};

export default Eye;
