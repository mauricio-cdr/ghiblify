import React from 'react';
import { shallow } from 'enzyme';
import { PeopleContainer } from './PeopleContainer';

describe('<LocationsContainer />', () => {
  test('renders without crashing', () => {
    const props = {
      getLocations: a => a,
      match: { params: { id: '2' } },
      history: {},
      people: [],
      getPeople: a => a,
    };
    const component = shallow(
      <PeopleContainer {...props} />,
    );
    expect(component).toMatchSnapshot();
  });
});
