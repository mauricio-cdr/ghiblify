import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getPeople as fetchPeople } from './people.actions';
import { Grid } from '../../components/Layout';
import PersonCard from './components/PersonCard';
import './People.scss';

export class PeopleContainer extends React.Component {
  componentDidMount() {
    const { getPeople } = this.props;
    getPeople();
  }

  handleOnClickCard = person => () => {
    const { history } = this.props;
    history.push(`/people/${person.id}`);
  }

  renderPeople = (people) => {
    if (!people) return null;
    return people.map(p => (
      <Grid item key={p.id} xs>
        <PersonCard data={p} onClick={this.handleOnClickCard} />
      </Grid>
    ));
  }

  render() {
    const { people } = this.props;
    return (
      <Grid
        container
        direction="row"
        justify="space-around"
      >
        {this.renderPeople(people)}
      </Grid>
    );
  }
}

PeopleContainer.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }).isRequired,
  history: PropTypes.shape({}).isRequired,
  people: PropTypes.arrayOf(PropTypes.shape({})),
  getPeople: PropTypes.func.isRequired,
};

PeopleContainer.defaultProps = {
  people: [],
};

const mapStateToProps = state => ({
  people: state.get('peopleState').people,
  loading: state.get('peopleState').loading,
});

const mapDispatchToProps = dispatch => ({
  getPeople: () => dispatch(fetchPeople()),
});
export default connect(mapStateToProps, mapDispatchToProps)(PeopleContainer);
