import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Skeleton from 'react-loading-skeleton';
import { getResoulrceById as fetchResoulrceById } from './info-detail.actions';
import { Grid, FlexBox } from '../../components/Layout';
import Text from '../../components/Text/Text';
import Logo from '../../components/Logo/GhibliLogoLight';
import Button from '../../components/Button';
import { GHIBLI_WIKI_URL } from '../../constants/sites';
import { resources } from '../../helpers/selectors/models';
import './InfoDetail.scss';


export class InfoDetailContainer extends React.Component {
  componentDidMount() {
    const { getResoulrceById, match } = this.props;
    getResoulrceById({ params: { resource: match.params.resource, id: match.params.id } });
  }

  handleClickOption = () => () => {
  }

  handleOpenTab = () => {
    const { resource } = this.props;
    window.open(`${GHIBLI_WIKI_URL}${resource.title || resource.name}`, '_blank', 'width=700,height=700');
  }

  renderInfo = (item, loading) => {
    const { match: { params: { resource } } } = this.props;
    const cleanItem = resources[resource](item);
    const params = Object.keys(cleanItem);
    return (
      !loading
        ? (
          <React.Fragment>
            <Text variant="h2" color="secondary">
              {item.title || item.name}
            </Text>
            {params && item && params.map(param => (
              <Text variant="h4" key={param}>
                {`${param} : ${item[param]}`}
              </Text>
            ))}
          </React.Fragment>
        )
        : (
          <Grid item xs={5} lg={5} md={5}>
            <Skeleton count={4} />
          </Grid>
        )
    );
  }

  renderDescription = (resource, loading) => (
    !loading
      ? (
        <Text variant="h4">
          {resource.description}
        </Text>
      )
      : <Skeleton count={4} />
  )

  render() {
    const { resource, loading } = this.props;
    return (
      <FlexBox flexDirection="column" className="container">
        <Grid container>
          <Grid item xs={12} md={6}>
            <Logo className="FilmDetail-img" />
          </Grid>
          <Grid item xs={12} md={6}>
            <FlexBox
              display="flex"
              flexDirection="column"
              justifyContent="flex-end"
              className="FilmDetail-info"
            >
              {this.renderInfo(resource, loading)}
            </FlexBox>
          </Grid>
        </Grid>
        <Grid item xs className="FilmDetail-description">
          {this.renderDescription(resource, loading)}
        </Grid>
        <div className="FilmDetail-view-wiki">
          <Button onClick={this.handleOpenTab} variant="contained" color="secondary">
              GHIBLI WIKI
          </Button>
        </div>
      </FlexBox>
    );
  }
}

InfoDetailContainer.propTypes = {
  getResoulrceById: PropTypes.func.isRequired,
  resource: PropTypes.shape({}).isRequired,
  history: PropTypes.shape({}).isRequired,
  loading: PropTypes.bool.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
      resource: PropTypes.string,
    }),
  }).isRequired,
};

const mapStateToProps = state => ({
  resource: state.get('searchState').resource,
  loading: state.get('searchState').isLoadingResource,
});

const mapDispatchToProps = dispatch => ({
  getResoulrceById: req => dispatch(fetchResoulrceById(req)),
});
export default connect(mapStateToProps, mapDispatchToProps)(InfoDetailContainer);
