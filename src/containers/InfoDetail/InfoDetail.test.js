import React from 'react';
import { shallow } from 'enzyme';
import { InfoDetailContainer } from './InfoDetail';

describe('<InfoDetailContainer />', () => {
  xtest('renders without crashing', () => {
    const props = {
      getResoulrceById: a => a,
      resource: { match: { params: { resource: '' } } },
      history: {},
      loading: false,
      match: { params: { resourse: 'other' } },
    };
    const component = shallow(
      <InfoDetailContainer {...props} />,
    );
    expect(component).toMatchSnapshot();
  });
});
