import { fetchResourceById } from '../../services/search.service';
import * as ACTIONS from '../../constants/actions/search.action-types';

export const getResoulrceById = req => ({
  types: [
    ACTIONS.LOADING_FETCH_RESOURCE_BY_ID,
    ACTIONS.SUCCESS_FETCH_RESOURCE_BY_ID,
    ACTIONS.ERROR_FETCH_RESOURCE_BY_ID,
  ],
  promise: fetchResourceById(req),
});
