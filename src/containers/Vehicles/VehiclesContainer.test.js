import React from 'react';
import { shallow } from 'enzyme';
import { VehiclesContainer } from './VehiclesContainer';

describe('<VehiclesContainer />', () => {
  test('renders without crashing', () => {
    const props = {
      getVehicles: a => a,
      vehicles: [],
      history: {},
    };
    const component = shallow(
      <VehiclesContainer {...props} />,
    );
    expect(component).toMatchSnapshot();
  });
});
