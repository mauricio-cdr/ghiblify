import * as actions from './vehicles.actions';
import * as service from '../../services/vehicle.service';
import * as ACTIONS from '../../constants/actions/vehicles.action-types';

describe('Vehicles actions', () => {
  it('should create a getVehicles action', () => {
    const expectedAction = {
      types: [
        ACTIONS.LOADING_FETCH_VEHICLES,
        ACTIONS.SUCCESS_FETCH_VEHICLES,
        ACTIONS.ERROR_FETCH_VEHICLES,
      ],
      promise: service.fetchVehicles(),
    };
    expect(actions.getVehicles()).toEqual(expectedAction);
  });

  it('should create a search vehicles action', () => {
    const expectedAction = {
      types: [
        ACTIONS.LOADING_FETCH_VEHICLES,
        ACTIONS.SUCCESS_FETCH_VEHICLES,
        ACTIONS.ERROR_FETCH_VEHICLES,
      ],
      promise: service.fetchVehicles(),
    };
    expect(actions.searchVehicles()).toEqual(expectedAction);
  });
});
