import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import PropTypes from 'prop-types';
import Button from '../../../components/Button';
import TextIntl from '../../../components/Text/TextInt';

import Text from '../../../components/Text/Text';
import Card from '../../../components/Card';
import t from '../../../helpers/format/translate';

const useStyles = makeStyles({
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  pos: {
    marginBottom: 5,
  },
  release: {
    color: 'green',
  },
  media: {
    height: 140,
  },
});

const urlImg = param => `static/media/${param}.jpg`;


const VehicleCard = ({ data, onClick }) => {
  const classes = useStyles();
  return (
    <Card>
      <CardMedia
        className={classes.media}
        image={urlImg(data.id)}
        title={data.id}
      />
      <CardContent>
        <Text variant="h2" color="textSecondary" gutterBottom>
          {data.name}
        </Text>
        <Text className={classes.pos} variant="h5" color="textSecondary">
          {`${t('vehicles.card.description')}: ${data.description}`}
        </Text>
        <Text className={classes.pos} variant="h5" color="textSecondary">
          {`${t('vehicles.card.vehicle_class')}: ${data.vehicle_class} - ${t('vehicles.card.length')}: ${data.length}`}
        </Text>
      </CardContent>
      <CardActions>
        <Button size="medium" color="secondary" onClick={onClick(data)}>
          <TextIntl lkey="locations.card.read-more" onClick={onClick(data)} />
        </Button>
      </CardActions>
    </Card>
  );
};

VehicleCard.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    vehicle_class: PropTypes.string,
    length: PropTypes.string,
    url: PropTypes.string,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default VehicleCard;
