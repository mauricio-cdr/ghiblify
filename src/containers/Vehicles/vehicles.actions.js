import { fetchVehicles } from '../../services/vehicle.service';
import * as ACTIONS from '../../constants/actions/vehicles.action-types';

export const getVehicles = () => ({
  types: [
    ACTIONS.LOADING_FETCH_VEHICLES,
    ACTIONS.SUCCESS_FETCH_VEHICLES,
    ACTIONS.ERROR_FETCH_VEHICLES,
  ],
  promise: fetchVehicles(),
});

export const searchVehicles = () => ({
  types: [
    ACTIONS.LOADING_FETCH_VEHICLES,
    ACTIONS.SUCCESS_FETCH_VEHICLES,
    ACTIONS.ERROR_FETCH_VEHICLES,
  ],
  promise: fetchVehicles(),
});
