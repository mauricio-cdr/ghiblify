import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Grid } from '../../components/Layout';
import VehicleCard from './components/VehicleCard';


import { getVehicles as fetchVehicles } from './vehicles.actions';

export class VehiclesContainer extends React.Component {
  componentDidMount() {
    const { getVehicles } = this.props;
    getVehicles();
  }

  handleOnClickCard = vehicle => () => {
    const { history } = this.props;
    history.push(`/vehicles/${vehicle.id}`);
  }

  renderVehicles = (vehicles) => {
    if (!vehicles) return null;

    return vehicles.map(vehicle => (
      <Grid item key={vehicle.id} xs>
        <VehicleCard data={vehicle} onClick={this.handleOnClickCard} />
      </Grid>
    ));
  }

  render() {
    const { vehicles } = this.props;
    return (
      <Grid
        container
        direction="row"
        justify="space-around"
      >
        {this.renderVehicles(vehicles)}
      </Grid>
    );
  }
}

VehiclesContainer.propTypes = {
  getVehicles: PropTypes.func.isRequired,
  vehicles: PropTypes.arrayOf(PropTypes.shape({})),
  history: PropTypes.shape({}).isRequired,
};


VehiclesContainer.defaultProps = {
  vehicles: [],
};

const mapStateToProps = state => ({
  vehicles: state.get('vehiclesState').vehicles,
  isLoading: state.get('vehiclesState').isLoading,
});

const mapDispatchToProps = dispatch => ({
  getVehicles: () => dispatch(fetchVehicles()),
});
export default connect(mapStateToProps, mapDispatchToProps)(VehiclesContainer);
