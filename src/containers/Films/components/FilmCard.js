import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Card from '../../../components/Card';
import Text from '../../../components/Text/Text';
import TextIntl from '../../../components/Text/TextInt';
import t from '../../../helpers/format/translate';


const useStyles = makeStyles({
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  pos: {
    marginBottom: 12,
  },
  release: {
    color: 'green',
  },
});


const FilmCard = ({ data, onClick }) => {
  const classes = useStyles();
  const limitCharacters = 240;

  return (
    <Card>
      <CardContent>
        <Text variant="h2" color="textSecondary" gutterBottom>
          {data.title}
        </Text>
        <Text variant="h5" component="h2" color="secondary">
          {`${data.release_date} * Rotten tomatoes: ${data.rt_score} %`}
        </Text>
        <Text variant="h5" color="textSecondary">
          {`${t('films.card.director')}: ${data.director}`}
        </Text>
        <Text className={classes.pos} variant="h5" color="textSecondary">
          {`${t('films.card.producer')}: ${data.producer}`}
        </Text>
        <Text variant="body2" component="p">
          {`${data.description ? data.description.substring(0, limitCharacters) : '-'} ${data.description.length > limitCharacters && '...'}  `}
        </Text>
      </CardContent>
      <CardActions>
        <Button size="medium" color="secondary" onClick={onClick(data)}>
          <TextIntl lkey="films.card.read-more" />
        </Button>
      </CardActions>
    </Card>
  );
};

FilmCard.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    release_date: PropTypes.string,
    rt_score: PropTypes.string,
    director: PropTypes.string,
    producer: PropTypes.string,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default FilmCard;
