import { fetchFilms } from '../../services/film.service';
import * as ACTIONS from '../../constants/actions/films.action-types';

export const getFilms = () => ({
  types: [
    ACTIONS.LOADING_FETCH_FILMS,
    ACTIONS.SUCCESS_FETCH_FILMS,
    ACTIONS.ERROR_FETCH_FILMS,
  ],
  promise: fetchFilms(),
});
