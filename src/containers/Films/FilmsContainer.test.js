import React from 'react';
import { shallow } from 'enzyme';
import { FilmsContainer } from './FilmsContainer';

describe('<FilmsContainer />', () => {
  test('renders without crashing', () => {
    const props = {
      getFilms: a => a,
      films: [],
      history: {},
    };
    const app = shallow(
      <FilmsContainer {...props} />,
    );
    expect(app).toMatchSnapshot();
  });
});
