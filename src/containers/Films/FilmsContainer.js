import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getFilms as fetchFilms } from './films.actions';
import { Grid } from '../../components/Layout';
import FilmCard from './components/FilmCard';

export class FilmsContainer extends React.Component {
  componentDidMount() {
    const { getFilms } = this.props;
    getFilms();
  }

  handleCardClick = film => () => {
    const { history } = this.props;
    history.push(`/films/${film.id}`);
  }

  renderFilms = (films) => {
    if (!films) return null;
    return films.map(film => (
      <Grid item key={film.id} xs>
        <FilmCard data={film} onClick={this.handleCardClick} />
      </Grid>
    ));
  }

  render() {
    const { films } = this.props;
    return (
      <Grid
        container
        direction="row"
        justify="space-around"
      >
        {this.renderFilms(films)}
      </Grid>
    );
  }
}

FilmsContainer.propTypes = {
  getFilms: PropTypes.func.isRequired,
  films: PropTypes.arrayOf(PropTypes.shape({})),
  history: PropTypes.shape({}).isRequired,
};

FilmsContainer.defaultProps = {
  films: [],
};

const mapStateToProps = state => ({
  films: state.get('filmsState').films,
  loading: state.get('filmsState').loading,
});

const mapDispatchToProps = dispatch => ({
  getFilms: () => dispatch(fetchFilms()),
});
export default connect(mapStateToProps, mapDispatchToProps)(FilmsContainer);
