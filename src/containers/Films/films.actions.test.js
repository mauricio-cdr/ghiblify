import * as actions from './films.actions';
import { fetchFilms } from '../../services/film.service';
import * as ACTIONS from '../../constants/actions/films.action-types';


describe('Films actions', () => {
  it('should create a getFilms action', () => {
    const expectedAction = {
      types: [
        ACTIONS.LOADING_FETCH_FILMS,
        ACTIONS.SUCCESS_FETCH_FILMS,
        ACTIONS.ERROR_FETCH_FILMS,
      ],
      promise: fetchFilms(),
    };
    expect(actions.getFilms()).toEqual(expectedAction);
  });
});
