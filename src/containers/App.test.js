import React from 'react';
import { shallow } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';
import { App } from './App';


describe('<App />', () => {
  test('renders without crashing', () => {
    const props = {
      getResources: a => a,
    };
    const app = shallow(
      <Router>
        <App {...props}><div /></App>
      </Router>,
    );
    expect(app).toMatchSnapshot();
  });
});
