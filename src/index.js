import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Map } from 'immutable';
import './i18n';
import './index.css';
import Router from './routes';
import createStore from './store';
import * as serviceWorker from './serviceWorker';

require('dotenv').config();

const { store } = createStore(Map(window.REDUX_STATE));
ReactDOM.render(<Provider store={store}>{<Router />}</Provider>, document.getElementById('root'));
serviceWorker.unregister();
