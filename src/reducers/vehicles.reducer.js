import * as ACTIONS from '../constants/actions/vehicles.action-types';

const initialState = {
  vehicles: [],
  isLoading: false,
};

const vehiclesReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LOADING_FETCH_VEHICLES:
      return { ...state, isLoading: true };
    case ACTIONS.SUCCESS_FETCH_VEHICLES:
      return { ...state, vehicles: action.result.data, isLoading: false };
    default:
      return state;
  }
};

export default vehiclesReducer;
