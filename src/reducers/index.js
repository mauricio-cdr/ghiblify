import {
  combineReducers,
} from 'redux-immutable';

import receibeResources from '../high-order-reducers/resources.reducer';
import searchState from './search.reducer';
import errorsState from './errors.reducer';
import filmsState from './films.reducer';
import homeState from './home.reducer';
import locationsState from './locations.reducer';
import notificationsState from './notifications.reducer';
import peopleState from './people.reducer';
import speciesState from './species.reducer';
import vehiclesState from './vehicles.reducer';

export default combineReducers({
  searchState,
  errorsState,
  homeState,
  notificationsState,
  filmsState: receibeResources('films', filmsState),
  locationsState: receibeResources('locations', locationsState),
  peopleState: receibeResources('people', peopleState),
  speciesState: receibeResources('species', speciesState),
  vehiclesState: receibeResources('vehicles', vehiclesState),
});
