import * as ACTIONS from '../constants/actions/people.action-types';

const initialState = {
  isLoading: false,
  people: [],
};

const peopleReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LOADING_FETCH_PEOPLE:
      return { ...state, isLoading: true };
    case ACTIONS.SUCCESS_FETCH_PEOPLE:
      return { ...state, isLoading: false, people: action.result.data };
    case ACTIONS.ERROR_FETCH_PEOPLE:
      return { ...state, isLoading: false };
    default:
      return state;
  }
};

export default peopleReducer;
