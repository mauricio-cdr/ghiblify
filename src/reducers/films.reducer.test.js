import reducer from './films.reducer';

describe('FilmsReducer', () => {
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      films: [],
      film: {},
      isLoadingFilm: false,
      isLoadingList: false,
    });
  });
});
