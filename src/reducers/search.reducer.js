import * as ACTIONS from '../constants/actions/search.action-types';

const initialState = {
  isLoadingResults: false,
  errorResults: null,
  results: [],
  resource: {},
  isLoadingResource: false,
};

const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LOADING_FETCH_SEARCH:
      return { ...state, isLoadingResults: true };
    case ACTIONS.SUCCESS_FETCH_SEARCH:
      return { ...state, isLoadingResults: false, results: action.result.data };
    case ACTIONS.ERROR_FETCH_SEARCH:
      return { ...state, isLoadingResults: false };
    case ACTIONS.LOADING_FETCH_RESOURCE_BY_ID:
      return { ...state, resource: {}, isLoadingResource: true };
    case ACTIONS.SUCCESS_FETCH_RESOURCE_BY_ID:
      return { ...state, resource: action.result.data, isLoadingResource: false };
    default:
      return state;
  }
};

export default searchReducer;
