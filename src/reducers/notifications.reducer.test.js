import reducer from './notifications.reducer';

describe('NotificationsReducer', () => {
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      loading: false,
    });
  });
});
