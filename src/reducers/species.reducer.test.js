import reducer from './species.reducer';

describe('SpeciesReducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      species: [],
      isLoading: false,
    });
  });
});
