import reducer from './people.reducer';

describe('PeopleReducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      isLoading: false,
      people: [],
    });
  });
});
