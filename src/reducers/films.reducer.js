import * as ACTIONS from '../constants/actions/films.action-types';

const initialState = {
  films: [],
  film: {},
  isLoadingFilm: false,
  isLoadingList: false,
};

const filmsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LOADING_FETCH_FILMS:
      return { ...state, isLoadingList: true };
    case ACTIONS.SUCCESS_FETCH_FILMS:
      return { ...state, films: action.result.data, isLoadingList: false };
    case ACTIONS.LOADING_FETCH_FILM_BY_ID:
      return { ...state, isLoadingFilm: true };
    case ACTIONS.SUCCESS_FETCH_FILM_BY_ID:
      return { ...state, film: action.result.data, isLoadingFilm: false };
    case ACTIONS.ERROR_FETCH_FILM_BY_ID:
    default:
      return state;
  }
};

export default filmsReducer;
