import reducer from './home.reducer';

describe('HomeReducer', () => {
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      loading: false,
    });
  });
});
