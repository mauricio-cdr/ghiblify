import * as ACTIONS from '../constants/actions/locations.action-types';

const initialState = {
  locations: [],
  isLoading: false,
};

const locationsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LOADING_FETCH_LOCATIONS:
      return { ...state, isLoading: true };
    case ACTIONS.SUCCESS_FETCH_LOCATIONS:
      return { ...state, locations: action.result.data, isLoading: false };
    default:
      return state;
  }
};

export default locationsReducer;
