import reducer from './vehicles.reducer';

describe('VehiclesReducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      vehicles: [],
      isLoading: false,
    });
  });
});
