const LOADING_PAGE_ABOUT = 'LOADING_PAGE_ABOUT';

const initialState = {
  loading: false,
};

const notificationsReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_PAGE_ABOUT:
      return { ...state, loading: true };
    default:
      return state;
  }
};

export default notificationsReducer;
