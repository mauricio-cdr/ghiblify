import * as ACTIONS from '../constants/actions/species.action-types';

const initialState = {
  species: [],
  isLoading: false,
};

const speciesReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LOADING_FETCH_SPECIES:
      return { ...state, isLoading: true };
    case ACTIONS.SUCCESS_FETCH_SPECIES:
      return { ...state, species: action.result.data, isLoading: false };
    default:
      return state;
  }
};

export default speciesReducer;
