import reducer from './locations.reducer';

describe('LocationsReducer', () => {
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      locations: [],
      isLoading: false,
    });
  });
});
