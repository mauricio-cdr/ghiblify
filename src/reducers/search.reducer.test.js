import reducer from './search.reducer';

describe('SearchReducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      isLoadingResults: false,
      errorResults: null,
      results: [],
      resource: {},
      isLoadingResource: false,
    });
  });
});
