import reducer from './errors.reducer';

describe('ErrosReducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      errors: [],
    });
  });
});
