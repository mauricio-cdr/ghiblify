import { ERROR_FETCH_LOCATIONS } from '../constants/actions/locations.action-types';

const initialState = {
  errors: [],
};

const errorsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ERROR_FETCH_LOCATIONS: {
      const { errors } = state;
      errors.push(action.error);
      return { ...state, errors };
    }
    default:
      return state;
  }
};

export default errorsReducer;
