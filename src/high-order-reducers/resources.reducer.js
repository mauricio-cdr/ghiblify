
const withPagination = (resource, reducer) => (state, action) => {
  switch (action.type) {
    case 'SUCCESS_FETCH_RESOURCES': {
      const results = action.result.filter(res => res.resource === resource);
      return { ...state, [resource]: results[0].data, isLoading: false };
    }
    // ...
    default:
      return reducer(state, action);
  }
};

export default withPagination;
